package com.example.models.spinneret;

import java.util.List;

/**
 * Created by metroide on 01/07/2017.
 */

public interface SpenneretInterface {

    /**
     * get all spenneret avalaible in the aplication
     * @return
     */

    public List<SpenneretModel> getAllSpennert();

    /**
     * get avalaible spenneret using id
     * @return
     */
    public SpenneretModel getSpenneretById(long _id);


}

package com.example.models.spinneret;

import android.provider.BaseColumns;

/**
 * Created by metroide on 01/07/2017.
 */

public class SpinneretContrat {
    public static final class SpinneretEntity implements BaseColumns {
        public static final String
            EntityName = "spinneret";
        public static final String
            AttributeName = "wording";
        public static final String
            AttributeID = "_id";
    }
}

package com.example.models.mark;

import com.example.models.level.LevelModel;
import com.example.models.student.StudentModel;

import java.util.List;

/**
 * Created by metroide on 01/07/2017.
 */

public interface MarkInterface {
    /** get all available mark for one student**/
    public List<MarkModel> getAllMark(StudentModel student);

    /**get all available mark for student in one level**/
    public List<MarkModel> getallMarkByLevel(StudentModel studentModel, LevelModel level);
}

package com.example.models.mark;

/**
 * Created by metroide on 01/07/2017.
 */

public class MarkModel {
    private long _id;
    private int credit;
    private float mark;
    private String matriculeStudent;
    private String levelID;


    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public float getMark() {
        return mark;
    }

    public void setMark(float mark) {
        this.mark = mark;
    }

    public String getMatriculeStudent() {
        return matriculeStudent;
    }

    public void setMatriculeStudent(String matriculeStudent) {
        this.matriculeStudent = matriculeStudent;
    }

    public String getLevelID() {
        return levelID;
    }

    public void setLevelID(String levelID) {
        this.levelID = levelID;
    }
}

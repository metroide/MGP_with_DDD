package com.example.models.mark;

import android.provider.BaseColumns;

/**
 * Created by metroide on 01/07/2017.
 */

public class MarkContrat {
    public static final class MarkEntity implements BaseColumns {
        public static final String
                EntityName = "marks";
        public static final String
                AttributeMark = "value";
        public static final String
                AttributeCredit = "credit";
        public static final String
                AttributeID = "_id";
        public static final String
                AttributeStudentId = "_id_student";
        public static final String
                AttributeLevelId = "_id_level";
    }
}

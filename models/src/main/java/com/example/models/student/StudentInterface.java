package com.example.models.student;

import java.util.List;

/**
 * Created by metroide on 01/07/2017.
 */

public interface StudentInterface {
    /** get all available student**/
    public List<StudentModel> getAllStudent();

    /**get information about single user **/
    public StudentModel getStudentInformation(StudentModel student);

}

package com.example.models.student;

import android.provider.BaseColumns;

/**
 * Created by metroide on 01/07/2017.
 */

public class StudentContrat {
    public static final class StudentEntity implements BaseColumns {
        public static final String
                EntityName = "student";
        public static final String
                AttributeName = "name";
        public static final String
                AttributeSubName = "name";
        public static final String
                AttributeMatricule = "_id";

    }
}

package com.example.models.student;

import com.example.models.level.LevelModel;
import com.example.models.mark.MarkModel;

import java.util.List;

/**
 * Created by metroide on 01/07/2017.
 */

public class StudentModel {
    private String matricule;
    private String name;
    private String subname;
    private StudentModel studentModel;
    private List<LevelModel> levelModels;
    private List<MarkModel> markModels;

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubname() {
        return subname;
    }

    public void setSubname(String subname) {
        this.subname = subname;
    }

    public StudentModel getStudentModel() {
        return studentModel;
    }

    public void setStudentModel(StudentModel studentModel) {
        this.studentModel = studentModel;
    }

    public List<LevelModel> getLevelModels() {
        return levelModels;
    }

    public void setLevelModels(List<LevelModel> levelModels) {
        this.levelModels = levelModels;
    }

    public List<MarkModel> getMarkModels() {
        return markModels;
    }

    public void setMarkModels(List<MarkModel> markModels) {
        this.markModels = markModels;
    }
}

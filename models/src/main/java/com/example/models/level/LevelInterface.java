package com.example.models.level;

import com.example.models.student.StudentModel;

import java.util.List;

/**
 * Created by metroide on 01/07/2017.
 */

public interface LevelInterface {
    /**
     * get all level available on database
     * @return
     */
    public List<LevelModel> getAllLevel();

    /**get all level for one user **/
    public List<LevelModel> getAllLevelForStudent(StudentModel student);

}

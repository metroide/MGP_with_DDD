package com.example.models.level;

/**
 * Created by metroide on 01/07/2017.
 */

public class LevelModel {
    private long _id;
    private String value;

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

package com.example.models.level;

import android.provider.BaseColumns;

/**
 * Created by metroide on 01/07/2017.
 */

public class LevelContrat {
    public static final class LevelOjectValue implements BaseColumns {
        public static final String
                ObjectValueName = "level";
        public static final String
                AttributeValue = "value";
        public static final String
                AttributeID = "_id";
    }
}
